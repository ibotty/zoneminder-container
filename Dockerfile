FROM registry.fedoraproject.org/fedora:33
LABEL MAINTAINER="Tobias Florek <tf@schaeffer-ag.de>" \
      io.openshift.wants="mariadb" \
      io.k8s.description=""


RUN dnf install -y "https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm" \
 && dnf install -y zoneminder-nginx procps-ng \
 && dnf clean all \
 && sed -i '/^Requires=/s/mariadb.service//' /usr/lib/systemd/system/zoneminder.service \
 && systemctl enable zoneminder nginx fcgiwrap@nginx.socket fcgiwrap@nginx.service

ADD root/ /

ENTRYPOINT ["/sbin/init"]
