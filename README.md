# Zoneminder in a container

This image is a not self-contained installation of zoneminder without Event Notification and Machine Learning.
It requires a separate MariaDB database.


## Configuring

Zoneminder can be configured with environment variables.
Any environment variable beginning with `TM` will be set in the configuration file.
